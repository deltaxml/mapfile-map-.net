# Mapfile Map Sample
*The instructions here assume that the sample resources have been checked-out, cloned or downloaded and unzipped into the samples directory of the DITA-Compare release. The resources should be located such that they are two levels below the top level release directory that contains the exe files.*

*For example `DeltaXML-DITA-Compare-8_0_0_n/samples/sample-name`.*

---

## Summary

This sample compares two DITA map files in1.ditamap and in2.ditamap, it does not compare the referenced topics. Because of this, the topic files are not included in the sample. Please read the content of the DITA markup result (results/dita-markup.ditamap) as it is designed to illustrate several of our DITA comparison product's features. For more details see: [Mapfile Map documentation](https://docs.deltaxml.com/dita-compare/latest/mapfile-map-sample-8290419.html).

## Using a Batch File

Run the rundemo.bat batch file either by entering rundemo from the command-line or by double-clicking on this file from Windows Explorer. This script runs the same comparison 4 times, once for each output format. The output destination is a freshly created results directory.

## Running the sample from the Command line

The sample can be run directly by issuing the following command

    ..\..\bin\deltaxml-dita.exe compare mapfile in1.ditamap in2.ditamap out.ditamap output-format=dita-markup
    
Where `dita-markup` format enumeration can be replaced by one of the tracked changes format enumerations.